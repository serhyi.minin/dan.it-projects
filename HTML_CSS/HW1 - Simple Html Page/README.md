# HW1 - Simple HTML Page

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Contributing](#contributing)

## About <a name = "about"></a>

It is my the very first homework for DAN.IT.

## Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

Any browser fits you, I prefer Chrome

### Installing

git clone https://gitlab.com/serhyi.minin/dan.it-projects.git

## Usage <a name = "usage"></a>

Any suggestions are welcomed!

## Contributing <a name = "contributing"></a>

DAN.IT Education
