/*
Технические требования:

При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
Поведение поля должно быть следующим:

При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.

В папке img лежат примеры реализации поля ввода и создающегося span.
*/
const input = document.querySelector(`.input_box`);
const label = document.querySelector(`.input_label`);
const cont = document.querySelector(`.container`);

input.addEventListener(`focus`, (event) => {
  event.target.style.border = `3px solid rgb(60,179,113)`;
  event.target.style.color = `inherit`;
});

input.addEventListener(`blur`, clearBorder);

function clearBorder(event) {
  event.target.style.border = `1px solid black`;
  let result = +input.value;
  console.log(result);

  const span = document.createElement("span");
  const x = document.createElement("button");
  const div = document.createElement("div");

  if (result < 0) {
    const spnExists = document.querySelector(`.spn`);
    input.style.border = `3px solid red`;

    if (spnExists === "undefined" || spnExists === null) {
      input.style.border = `3px solid red`;
      span.style.display = `block`;
      span.classList.add(`spn`);
      span.innerText = `Please enter correct price`;
      cont.append(span);
    }
  } else if (result > 0) {
    span.classList.add(`spn`);
    span.innerText = `Текущая цена: ${result}`;
    x.innerText = `X`;
    x.classList.add(`btn_x`);
    span.append(x);
    div.classList.add(`wrapper`);
    div.append(span);
    div.style.display = `flex`;
  } else if (result === `` || result === 0) {
    if (
      document.querySelector(`.wrapper`) !== "undefined" &&
      document.querySelector(`.wrapper`) !== null
    ) {
      document.querySelector(`.wrapper`).remove();
    } else if (
      document.querySelector(`.spn`) !== "undefined" &&
      document.querySelector(`.spn`) !== null
    ) {
      document.querySelector(`.spn`).remove();
    }
  }

  const wrapperExists = document.querySelector(`.wrapper`);
  if (wrapperExists === "undefined" || wrapperExists === null) {
    label.prepend(div);
    input.style.color = `green`;
  }
  x.addEventListener(`click`, (event) => {
    input.value = "";
    input.style.color = `inherit`;

    if (
      document.querySelector(`.wrapper`) !== "undefined" &&
      document.querySelector(`.wrapper`) !== null
    ) {
      document.querySelector(`.wrapper`).remove();
    } else if (
      document.querySelector(`.spn`) !== "undefined" &&
      document.querySelector(`.spn`) !== null
    ) {
      document.querySelector(`.spn`).remove();
    }
  });
}
