/**
 * При наведенні на блок 1 робити
 * блок 2 зеленим кольором
 * А при наведенні на блок 2 робити
 * блок 1 червоним кольором
 *
 */

const block1 = document.querySelector(`#block-1`);

block1.addEventListener(`mouseover`, colorGreen);
block1.addEventListener(`mouseout`, colorWhite);

const block2 = document.querySelector(`#block-2`);
block2.addEventListener(`mouseover`, colorRed);
block2.addEventListener(`mouseout`, changeColor);

function colorGreen() {
  block2.style.backgroundColor = `green`;
}
function colorRed() {
  block1.style.backgroundColor = `red`;
}
function colorWhite() {
  block2.style.backgroundColor = ``;
}
