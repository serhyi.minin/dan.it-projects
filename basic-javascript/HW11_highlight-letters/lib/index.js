/*
Технические требования:
В файле index.html лежит разметка для кнопок.
Каждая кнопка содержит в себе название клавиши на клавиатуре
По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.
*/
const keys = Array.from(document.querySelectorAll(`.btn`));
document.addEventListener(`keyup`, pressedKey);
let previousKey = null;

function pressedKey(e) {
  keys.find((item) => {
    let foundKey = e.code;
    e.code === `Enter`
      ? (foundKey = e.code)
      : (foundKey = e.code.substring(3, e.code.length));

    if (item.innerText === foundKey) {
      if (previousKey !== null) {
        previousKey.style.backgroundColor = ``;
      }
      item.style.backgroundColor = `blue`;
      previousKey = item;
    }
  });
}
