button.onclick = function () {
  /*Технические требования:

  Считать с помощью модального окна браузера два числа.
  Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
  Создать функцию, в которую передать два значения и операцию.
  Вывести в консоль результат выполнения функции.


  Необязательное задание продвинутой сложности:
  После ввода данных добавить проверку их корректности.
  Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

Некоректно поставлена задача. Вимагається отримати два числа за допомогою одного модального вікна (використана однина у реченні) та за умови введення невірних значень запитати два числа знов. В такому випадку зберігати введенні раніше значення немає сенсу. Зберігати значення має сенс, коли числа запитуються окремими модальними вікнами і одне може бути вірним, а інше ні. Вирішив задачу по першому варіанту, за допомогою одного модального вікна без збереження невірно введених даних.
За отримання такого завдання в реальності перед початком роботи над завданням обов'язково запитав би додаткові роз'яснення.
*/

  let input_numbers = 'Input numbers with space between';
  let input_operator = 'Input operator +, -, * or /';
  let first_number = 0;
  let second_number = 0;
  let arr = [];
  let operator = '';
  let result = 0;
  let flag = false;

  do {
    //getting two numbers from modal window
    flag = false;

    let data = getData(input_numbers);

    if (data === null || data === undefined || data === '') {
      //ESC pressed
      break;
    }

    if (isContainTwoNumbers(data) !== false) {
      let numbers = isContainTwoNumbers(data);
      first_number = Number(numbers[0]);
      second_number = Number(numbers[1]);

      flag = true;
    }
  } while (flag === false);

  function getData(string) {
    let data = prompt(string);

    if (data === '' || data === null || data === undefined) {
      return data;
    } else {
      data = data.trim();
      data = data.replace(/\s+/g, ' ');
      return data;
    }
  }

  if (
    first_number !== 0 &&
    second_number !== 0 &&
    !isNaN(first_number) &&
    !isNaN(second_number)
  ) {
    do {
      //getting math operator from modal window
      data = getData(input_operator);

      if (data === null) {
        break;
      }

      flag = false;

      if (data !== undefined || data !== null) {
        if (
          data.includes('+') ||
          data.includes('-') ||
          data.includes('*') ||
          data.includes('/')
        ) {
          operator = data.charAt(0);
          flag = true;
        } else {
          console.error("The math operator doesn't entered");
        }
      }
    } while (flag === false);
  }

  function isContainTwoNumbers(arr) {
    arr = arr.split(' ');
    first_number = Number(arr[0]);
    second_number = Number(arr[1]);

    if (isNaN(first_number) || isNaN(second_number)) {
      console.error("At least one of entered strings isn't a number");
      return false;
    } else {
      return arr;
    }
  }

  function doMath(num1, num2, opp) {
    //doing calculations
    let result;
    switch (opp) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        result = num1 / num2;
        break;
    }
    return result;
  }

  if (
    first_number !== 0 &&
    second_number !== 0 &&
    operator !== ''
  ) {
    result = doMath(first_number, second_number, operator);
    console.log('Result is: ' + result); //Logging result to console. End of task
  }
  // ESC pressed
};
