/*
Добавить в домашнее задание HTML/CSS №4 (Flex/Grid) различные эффекты с использованием jQuery

Технические требования:

1. Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы. (Done)

2. При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.(Done)

3. Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с фиксированным позиционированием. При клике на нее - плавно прокрутить страницу в самый верх. (Done)

4. Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() (прятать и показывать по клику) для данной секции. (Done)
*/

$(document).on("click", ".go_to_top", function (e) {
  e.preventDefault();
  $("body, html").animate({ scrollTop: 0 }, 100);
});

$(document).ready(function () {
  let btn = $("#go_to_top");

  $(window).scroll(function () {
    if ($(window).scrollTop() > $(window).height()) {
      btn.addClass("show");
    } else {
      btn.removeClass("show");
    }
  });
});

$(".btnh").click(function () {
  $(".top-rated").toggle(500);
});

$("#go_to_top").hover(
  function () {
    $(this).css("cursor", "pointer").attr("title", "Вгору.");
  },
  function () {
    $(this).css("cursor", "auto");
  }
);
