/*
Технические требования:

Взять любое готовое домашнее задание по HTML/CSS.
Добавить на макете кнопку "Сменить тему".
При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
Выбранная тема должна сохраняться и после перезагрузки страницы
*/

let root = document.documentElement;

window.onload = () => {
  let bodyColor = localStorage.getItem("body-current");
  let firstColor = localStorage.getItem("first-current");

  if (!bodyColor || !firstColor) {
    localStorage.setItem("body-next", `lightblue`);
    localStorage.setItem("first-next", `darkcyan`);
  } else {
    root.style.setProperty("--clr-body", bodyColor);
    root.style.setProperty("--clr-first", firstColor);
  }
};

const btn = document.querySelector("#changetheme");

btn.addEventListener(`click`, () => {
  let a = getComputedStyle(document.documentElement).getPropertyValue(
    "--clr-body"
  );
  let b = getComputedStyle(document.documentElement).getPropertyValue(
    "--clr-first"
  );

  let bodyColor = localStorage.getItem("body-next");
  let firstColor = localStorage.getItem("first-next");

  root.style.setProperty("--clr-body", bodyColor);
  root.style.setProperty("--clr-first", firstColor);

  localStorage.setItem("body-next", a);
  localStorage.setItem("first-next", b);

  localStorage.setItem("body-current", bodyColor);
  localStorage.setItem("first-current", firstColor);
});

const navContainer = document.querySelector(`.tabs`);
const navList = Array.from(document.querySelectorAll(`.tabs-title`));
const pList = Array.from(document.querySelectorAll(`.tabs-content p`));
let current = pList[0];
current.style.display = "block";

navContainer.addEventListener(`click`, (event) => {
  navList.forEach((elem) => {
    elem.classList.remove(`active`);
  });
  event.target.classList.add(`active`);
  let index = navList.indexOf(event.target);
  pList[index].style.display = "block";
  current.style.display = `none`;
  current = pList[index];
});
