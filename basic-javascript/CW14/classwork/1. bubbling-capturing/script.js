const eventPhases = {
  1: 'погружение',
  2: 'фаза цели',
  3: 'всплытие',
};

const section = document.getElementById('section');
const div = document.getElementById('div');
const button = document.getElementById('button');

// События срабатывают на этапе всплытия (bubble) (по умолчанию)
div.addEventListener('click', handler);
section.addEventListener('click', handler);
button.addEventListener('click', handler);
// третьим параметром можно указать false или { capture:false }, но это необязательно - false стоит по-умолчанию

// События срабатывают на этапе всплытия (bubble)
// Для обработчиков, добавленных через on<event>-свойство или через HTML-атрибуты
// div.onclick = handler;
// section.onclick = handler;
// button.onclick = handler;

// События срабатывают на этапе погружения (capture) (старая спецификация)
// div.addEventListener('click', handler, true);
// section.addEventListener('click', handler, true);
// button.addEventListener('click', handler, true);

// События срабатывают на этапе погружения (capture) (современная спецификация)
// div.addEventListener('click', handler, { capture: true });
// section.addEventListener('click', handler, { capture: true });
// button.addEventListener('click', handler, { capture: true });

function handler(event) {
  // event.stopPropagation();
  console.log(
    `event.target.tagName: ${event.target.tagName},
event.currentTarget.tagName: ${event.currentTarget.tagName},
event.eventPhase: ${event.eventPhase} (${eventPhases[event.eventPhase]})`,
  );
}
