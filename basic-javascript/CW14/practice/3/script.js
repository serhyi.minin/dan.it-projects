/**
 * Создайте галерею изображений, в которой основное изображение изменяется при клике на уменьшенный вариант.
 *
 * Используйте делегирование событий.
 */
const collect = document.getElementById(`thumbs`);
let set = document.querySelector(`#largeImg`);
let href = null;

collect.addEventListener(`click`, (event) => {
  event.preventDefault();
  href = event.target.src;
  set.src = href;
});
