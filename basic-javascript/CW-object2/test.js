//-------------------- Classwork Objects -------------------/
//Working with strings, date and time

/**
 * Задание 2.
 * Написать функцию, capitalizeAndDoublify, которая переводит
 * символы строки в верхний регистр и дублирует каждый её символ.
 *
 * Условия:
 * - Использовать встроенную функцию repeat;
 * - Использовать встроенную функцию toUpperCase;
 * - Использовать цикл for...of.
 */
/* Решение */

function capitalizeAndDoublify(string) {
  let str = "";
  for (let i of string) {
    str += i.repeat(2).toUpperCase();
  }
  return str;
}

function capitalizeAndDoublify2(string) {
  let str = "";
  for (let i = 0; i < string.length; i++) {
    string = string.toUpperCase();
    str += string[i].repeat(2);
  }
  return str;
}

/* Пример */
// console.log(capitalizeAndDoublify('hello')); // HHEELLLLOO
// console.log(capitalizeAndDoublify2('JavaScript!')); // JJAAVVAASSCCRRIIPPTT!!

/**
 * Задание 1.
 *
 * Написать имплементацию встроенной функции строки repeat(times).
 *
 * Функция должна обладать двумя параметрами:
 * - Целевая строка для повторения;
 * - Количество повторений целевой строки.
 *
 * Функция должна возвращать преобразованную строку.
 *
 * Условия:
 * - Генерировать ошибку, если первый параметр не является строкой,
 * а второй не является числом.
 */
/* Решение */
function repeat(str, qnt) {
  if (typeof str !== "string" || isNaN(Number(qnt))) {
    throw Error("First parameter is not a string or second is not a number");
  }
  let result = "";

  while (qnt !== 0) {
    //better use FOR loop provided for "number" has known.
    result += str;
    qnt--;
  }
  return result;
}
/* Пример */

/*
const string = "Hello, world!";
console.log(repeat(string, 3)); // Hello, world!Hello, world!Hello, world!
console.log(repeat(string, 1)); // Hello, world!
console.log(repeat(string, 2)); // Hello, world!Hello, world!
console.log(repeat(string, 5)); // Hello, world!Hello, world!Hello, world!Hello, world!Hello, world!
console.log(repeat(7, 5)); // Error: First parameter should be a string type.
*/

/**
 * Задание 4.
 *
 * Написать функцию-помощник кладовщика.
 *
 * Функция обладает одним параметром:
 * - Строка со списком товаров через запятую (water,banana,black,tea,apple).
 *
 * Функция возвращает строку в формате ключ-значение, где ключ — имя товара,
 * а значение — его остаток на складе, например:
 * apple: 8
 *
 * Каждый новый товар внутри строки должен содержатся на новой строке.
 *
 * Если какого-то товара на складе нет, в качестве остатка указать «not found».
 *
 * Условия:
 * - Имя товара не должны быть чувствительно к регистру;
 * - Дополнительных проверок совершать не нужно.
 */
/* Дано */
const store = {
  apple: 8,
  beef: 162,
  banana: 14,
  chocolate: 0,
  milk: 2,
  water: 16,
  coffee: 0,
  tea: 13,
  cheese: 0,
};
/* Решение */
function getStoreBalance(str) {
  arr = str.split(",");
  let stock = "";

  for (let item of arr) {
    let iter = item.toLowerCase();
    if (store.hasOwnProperty(iter)) {
      stock += item + " - " + store[iter] + "\n";
    } else {
      stock += item + " - " + "not found" + "\n";
    }
  }

  return stock;
}

console.log(getStoreBalance("Water,banana,black,tea,Apple"));
