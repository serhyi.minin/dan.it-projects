//-------------------- Classwork Arrays -------------------/
// 1. Отфильтровать всех кто старше 7000 лет.
// Оставить всех кто младше
// 2. Отфильтровать всех кто умер. (alive === false)
// 3. Вернуть массив имен.
const array = [
  {
    name: 'Bilbo',
    age: 50,
    alive: true,
  },
  {
    name: 'Nazgul',
    age: 1200,
    alive: false,
  },
  {
    name: 'Gandalf',
    age: 7777,
    alive: false,
  },
  {
    name: 'Aragorn',
    age: 86,
    alive: false,
  },
  {
    name: 'Galadriel',
    age: 8000,
    alive: true,
  },
];
console.log(
  array
    .filter((e) => e.age < 7000)
    .filter((e) => e.alive)
    .map((e) => e.name),
);
