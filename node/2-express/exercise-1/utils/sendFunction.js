export const sendGoodResponse = (res, data) => {
    const body = {
        status: "success",
        data,
    }

    res.status(200).json(body);
}

export const sendBadResponse = (res, status, message) => {
    const body = {
        status: "error",
        message,
    }

    res.status(status).json(body);
}