import React from 'react'
import { Field, Form, Formik } from 'formik'
import { Button, TextField } from '@material-ui/core'
import PropTypes from 'prop-types'
import {useHistory} from "react-router-dom";

const PostForm = (props) => {
	const history = useHistory();
	const { title, text, submitCallback } = props;

	const initialValue = {
		title,
		text,
	}

	const onSubmit = async (values, { resetForm }) => {
		console.log(values)
		const data = await fetch('http://localhost:8080/posts', {
			method: "POST",
			headers: {
				"content-type": "application/json"
			},
			body: JSON.stringify(values)
		}).then((res) => {
			if (res.status === 200) {
				history.push("/")
			}
		})
		resetForm();

		if (submitCallback) {
			submitCallback();
		}
	}

	return (
		<Formik onSubmit={onSubmit} initialValues={initialValue}>
			<Form>
				<Field
					name='title'
					label='Title'
					variant='outlined'
					className='input'
					as={TextField}
				/>

				<Field
					name='text'
					label='Text'
					variant='outlined'
					multiline
					minRows={3}
					className='input'
					as={TextField}
				/>

				<Button color="primary" variant="contained" type='submit'>Submit</Button>
			</Form>
		</Formik>
	)
}

PostForm.propTypes = {
	title: PropTypes.string,
	text: PropTypes.string,
	submitCallback: PropTypes.func,
}
PostForm.defaultProps = {
	title: '',
	text: '',
	submitCallback: null,
}

export default PostForm