import React from "react";
import PostContainer from '../../components/PostContainer'

function HomePage() {
    return (
        <section>
            <h1>POSTS</h1>
            <PostContainer />
        </section>
    )
}

export default HomePage;