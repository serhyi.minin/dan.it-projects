import getPosts from "./posts/getPosts.js";
import addNewPost from "./posts/addNewPost.js";
import deletePost from "./posts/deletePost.js";

const routes = app => {
    getPosts(app);
    addNewPost(app);
    deletePost(app)
};

export default routes;