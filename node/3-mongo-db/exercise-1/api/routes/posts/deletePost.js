import PostsModel from "../../mongoose/posts/PostsModel.js";

const deletePost = (app) => {
    app.delete('/posts', async (req, res) => {
        const { query } = req;
        console.log(query)

        if (!query?.id){
            res.status(400).send({ messageError: "Id is required" });
        }

        try {
            const data = await PostsModel.findByIdAndDelete(query?.id);
            console.log(data);

            const postData = await PostsModel.find();
            res.status(200).send(postData);
        } catch (error) {
            console.error(error);
            res.status(500).send({ messageError: "Something went wrong" })
        }

    })
};

export default deletePost;