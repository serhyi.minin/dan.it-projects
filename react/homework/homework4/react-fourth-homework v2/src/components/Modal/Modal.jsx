import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import PropTypes from "prop-types";
import styles from "./Modal.module.scss";
import { setIsOpenModal } from "../../store/actionCreators/actionCreator";

function Modal(props) {
  const { modalHeader, question, actions, className } = props;

  const dispatch = useDispatch();
  const closeModal = () => dispatch(setIsOpenModal(false));

  const handleClick = (e) => {
    //Line 26:6:  React Hook useEffect has a missing dependency: 'handleClick'. Either include it or remove the dependency array  react-hooks/exhaustive-deps
    if (e.target.className !== `${styles.main} ${className}`) {
      return;
    }
    closeModal();
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClick);
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, []);

  return (
    <div className={`${styles.main} ${className}`}>
      <div className={styles.box}>
        <div className={`${styles.header} ${className}`}>
          <span className={styles.title}>{modalHeader}</span>
          <div className={styles.close} onClick={closeModal}></div>
        </div>
        <p className={styles.text}>{question}</p>
        <div className={styles.buttons}>{actions}</div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  actions: PropTypes.object.isRequired,
  className: PropTypes.string,
  modalHeader: PropTypes.string,
  question: PropTypes.string,
};

export default Modal;
