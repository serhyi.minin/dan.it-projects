import PropTypes from "prop-types";
import React from "react";
import styles from "./Section.module.scss";
import CardList from "../CardList/CardList";
import SectionTitle from "../SectionTitle/SectionTitle";

const Section = (props) => {
  const { title } = props;

  return (
    <section className={styles.container}>
      <SectionTitle title={title} />
      <div className={styles.cards}>
        <CardList title={title} />
      </div>
    </section>
  );
};

export default Section;
