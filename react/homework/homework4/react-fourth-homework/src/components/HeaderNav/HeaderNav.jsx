import PropTypes from "prop-types";
import React, { useState } from "react";
import styles from "./HeaderNav.module.scss";
import Icon from "@mdi/react";
import { Link } from "react-router-dom";
import { mdiCartMinus, mdiHome, mdiStarOutline } from "@mdi/js";

const HeaderNav = (props) => {
  // const { cards, storedCards } = props;


  // let arr = cards.filter((elem) => elem.isFavorite === true);

  let favFlag = false;
  let cartFlag = false;
  // arr.length > 0 ? (favFlag = false) : (favFlag = true);
  // storedCards.length > 0 ? (cartFlag = false) : (cartFlag = true);

  return (
    <nav>
      <ul>
        <li>
          <Link to="/" className={styles.navigationLink}>
            <Icon path={mdiHome} title="Home" size={1} />
            Home
          </Link>
        </li>
        <li>
          <Link to="/favorite" className={styles.navigationLink}>
            {favFlag && <Icon path={mdiStarOutline} title="Empty" size={1} />}
            {!favFlag && (
              <Icon
                path={mdiStarOutline}
                title="Buy it!"
                size={1}
                color="red"
              />
            )}
            Favorites
          </Link>
        </li>
        <li>
          <Link to="/cart" className={styles.navigationLink}>
            {cartFlag && <Icon path={mdiCartMinus} title="Empty" size={1} />}
            {!cartFlag && (
              <Icon path={mdiCartMinus} title="Buy it!" size={1} color="red" />
            )}
            Cart
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default HeaderNav;
