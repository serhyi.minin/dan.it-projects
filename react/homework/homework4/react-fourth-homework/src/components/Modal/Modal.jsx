import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import styles from "./Modal.module.scss";
import { setIsOpenModal } from "../../store/actionCreators/actionCreator";

function Modal(props) {
  // const { closeModal, modalHeader, question, actions, className } = props;
  const { modalHeader, question, actions, className } = props;
  // const { isOpen, id, title } = useSelector((state) => state.modal);

  // const { isOpen } = useSelector((state) => state.modal);
  const dispatch = useDispatch();
  const closeModal = () => dispatch(setIsOpenModal(false));

  // if (!isOpen) return null;

  const handleClick = (e) => {
    if (e.target.className !== `${styles.main} ${className}`) {
      return;
    }
    // closeModal();
  };

  // useEffect(() => {
  //   document.addEventListener("mousedown", handleClick);
  //   return () => {
  //     document.removeEventListener("mousedown", handleClick);
  //   };
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  return (
    <div className={`${styles.main} ${className}`}>
      <div className={styles.box}>
        <div className={`${styles.header} ${className}`}>
          <span className={styles.title}>{modalHeader}</span>
          <div className={styles.close} onClick={closeModal}></div>
        </div>
        <p className={styles.text}>{question}</p>
        <div className={styles.buttons}>{actions}</div>
      </div>
    </div>
  );
}

export default Modal;
