import React from "react";
import Button from "./Button";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

const handleClick = jest.fn();

const Component = (props) => {
  return (
    <Button
      btnText="ButtonTest"
      onClick={handleClick}
      className="active"
      {...props}></Button>
  );
};

describe("Button render test", () => {
  test("should button render", () => {
    const { asFragment } = render(<Component />);
    expect(asFragment()).toMatchSnapshot();
  });
});

describe("Button", () => {
  test("eventHandler called on click", () => {
    render(<Component onClick={handleClick} role="button" />);
    userEvent.click(screen.getByRole("button"));
    expect(handleClick).toBeCalled();
  });
});
