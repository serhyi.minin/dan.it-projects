import { combineReducers } from "redux";
import cardReducer from "./cardReducer";
import cartReducer from "./cartReducer";
import modalReducer from "./modalReducer";
import checkoutReducer from "./checkoutReducer";

const reducer = combineReducers({
  card: cardReducer,
  cart: cartReducer,
  modal: modalReducer,
  checkout: checkoutReducer,
});
export default reducer;
