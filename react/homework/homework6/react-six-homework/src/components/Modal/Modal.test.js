import React, { useEffect } from "react";
import { render, screen } from "@testing-library/react";
import { Provider, useDispatch } from "react-redux";
import Modal from "./Modal";
import store from "../../store";
import { setIsOpenModal } from "../../store/actionCreators/actionCreator";

jest.mock("@mui/material", () => ({ Button: () => <span>SPAN</span> }));

const OpenModalButton = () => {
  const dispatch = useDispatch();

  return (
    <button onClick={() => dispatch(setIsOpenModal(true))}>Open modal</button>
  );
};

const Component = () => {
  return (
    <Provider store={store}>
      <OpenModalButton />
      <Modal />
    </Provider>
  );
};

describe("Modal snapshot render", () => {
  test("should modal render", () => {
    const { asFragment } = render(<Component />);

    expect(asFragment()).toMatchSnapshot();
  });
});

describe("Modal is open", () => {
  test("should modal open when store value is changed", () => {
    const { getByText, getByTestId } = render(<Component />);
    getByText("Open modal").click();
    expect(getByTestId("modal")).toBeInTheDocument();
  });
});
