import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styles from "./Modal.module.scss";

class Modal extends PureComponent {
  componentDidMount() {
    document.addEventListener("mousedown", this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClick, false);
  }

  handleClick = (e) => {
    if (this.node.contains(e.target)) {
      return;
    }
    this.props.closeModal();
  };

  render() {
    const { closeModal, header, text, actions, className } = this.props;

    return (
      <div className={`${styles.main} ${className}`}>
        <div ref={(node) => (this.node = node)} className={styles.box}>
          <div className={`${styles.header} ${className}`}>
            <span className={styles.title}>{header}</span>
            <div className={styles.close} onClick={closeModal}></div>
          </div>
          <p className={styles.text}>{text}</p>
          <div className={styles.buttons}>{actions}</div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  actions: PropTypes.element.isRequired,
  className: PropTypes.string,
  closeModal: PropTypes.func.isRequired,
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Modal;
