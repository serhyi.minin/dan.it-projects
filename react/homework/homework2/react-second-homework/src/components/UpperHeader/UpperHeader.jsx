import React from "react";
import PropTypes from "prop-types";
import styles from "./UpperHeader.module.scss";
import Icon from "@mdi/react";
import { mdiCartMinus } from "@mdi/js";

const UpperHeader = (props) => {
  const { isEmpty } = props;

  return (
    <div className={styles.wrap}>
      {isEmpty && <Icon path={mdiCartMinus} title="Empty" size={1} />}
      {!isEmpty && (
        <Icon path={mdiCartMinus} title="Go to buy" size={1} color="red" />
      )}
    </div>
  );
};

UpperHeader.propTypes = {
  isEmpty: PropTypes.bool,
};

export default UpperHeader;
