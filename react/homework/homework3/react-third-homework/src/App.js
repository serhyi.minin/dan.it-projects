import React, { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import styles from "./App.module.scss";
import Routes from "./Routes/Routes";
import Header from "./components/Header/Header";
import getCardsFromLS from "./functions/getCardsFromLS";
import setCardsToLS from "./functions/setCardsToLS";

function App() {
  const [cards, setCards] = useState(getCardsFromLS("Cards"));
  const [storedCards, setStoredCards] = useState(getCardsFromLS("Cart"));

  const addToCart = (id, closeModal) => {
    const index = cards.findIndex(({ id: arrayId }) => {
      return id === arrayId;
    });
    const newCartItem = cards[index];
    const oldCartItems = storedCards;

    oldCartItems.push(newCartItem);
    setCardsToLS("Cart", oldCartItems);
    setStoredCards(oldCartItems);
    closeModal();
  };

  const fetchFromServer = () => async () => {
    const { data } = await fetch("http://localhost:3000/goods.json").then(
      (res) => res.json()
    );
    setCards(data);
    setCardsToLS("Cards", data);
  };

  if (cards.length === 0) {
    fetchFromServer()();
  }

  const toggleFav = (id) => {
    const index = cards.findIndex(({ id: arrayId }) => {
      return id === arrayId;
    });
    cards[index].isFavorite = !cards[index].isFavorite;

    setCards((current) => {
      const newState = [...current];
      return newState;
    });
    setCardsToLS("Cards", cards);
  };

  const deleteFromCart = (id, closeModal) => {
    const index = storedCards.findIndex(({ id: arrayId }) => {
      return id === arrayId;
    });
    if (index > -1) {
      storedCards.splice(index, 1);
    }

    setStoredCards((current) => {
      const newState = [...current];
      return newState;
    });
    setCardsToLS("Cart", storedCards);
    closeModal();
  };

  return (
    <Router>
      <div className={styles.main}>
        <Header cards={cards} storedCards={storedCards} />
        <Routes
          addToCart={addToCart}
          toggleFav={toggleFav}
          cards={cards}
          storedCards={storedCards}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </Router>
  );
}

export default App;
