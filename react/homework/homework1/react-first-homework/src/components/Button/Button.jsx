import PropTypes from "prop-types";
import React, { Component } from "react";

class Button extends Component {
  render() {
    const { text, onClick, className } = this.props;

    return (
      <button className={className} onClick={onClick}>
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
};

export default Button;
