import React from 'react';
import {Formik, Form, Field, FastField, ErrorMessage} from "formik";
import CustomInput from "../CustomInput";
import * as yup from 'yup';
import CustomInputWithoutField from "../CustomInputWithoutField";

const FormWithFormik = () => {

    const handleSubmit = (values, actions) => {
        console.log(values)
        console.log(actions)
    }

    const initialValues = {
        name: '',
        email: '',
        password: '',
        city: '',
        fast: '',
    }

    const validationSchema = yup.object().shape({
        name: yup.string()
            .required('Name is required')
            .max(24, 'Name should be less then 24 letters')
            .matches(/^[A-Za-z]+$/gi, 'Name should consist of only latin letters'),
        email: yup.string().email('Invalid format of email').required('Email is required'),
        password: yup.string().min(6, 'Password should be more then 6 letters').required('Password is required'),
        city: yup.string().min(6, 'City should be more then 6 letters').required('City is required')
    })

    return (
        <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={validationSchema}>
            {(props) => {
                console.log(props);
                return (
                    <Form>
                        <h3>FORMIK</h3>
                        <Field
                            type="text"
                            name="name"
                            placeholder="Name"
                            label="Name"
                            component={CustomInput}
                        />
                        {/*<ErrorMessage name="name" render={msg => <span className='error'>{msg}</span>} />*/}

                        <Field
                            type="text"
                            name="email"
                            placeholder="Email"
                            label="Email"
                            component={CustomInput}
                        />
                        {/*<ErrorMessage name="email" render={msg => <span className='error'>{msg}</span>} />*/}

                        <Field
                            type="password"
                            name="password"
                            placeholder="Password"
                            label="Password"
                            component={CustomInput}
                        />
                        {/*<ErrorMessage name="password" render={msg => <span className='error'>{msg}</span>} />*/}

                        <CustomInputWithoutField
                            type="text"
                            name="city"
                            placeholder="City"
                            label="City"
                        />

                        <button type="submit">Submit</button>
                    </Form>
                )
            }}

        </Formik>
    )
}

export default FormWithFormik;