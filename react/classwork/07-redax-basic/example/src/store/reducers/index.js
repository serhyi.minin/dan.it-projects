import { combineReducers } from "redux";
import counterReducer from "./counterReducer";
import catFactsReducer from "./catFactsReducer";

const appReducer = combineReducers({
    counter: counterReducer,
    facts: catFactsReducer,
})

export default appReducer;