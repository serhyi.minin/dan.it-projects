import {ADD_FACT, SET_CAT_LOADING} from "../actions";

const initialState = {
    facts: [],
    isLoading: false,
}

const catFactsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_CAT_LOADING:
            return {...state, isLoading: payload};
        case ADD_FACT:
            return {...state, facts: [...state.facts, payload]};
        default:
            return state;
    }
}

export default catFactsReducer;