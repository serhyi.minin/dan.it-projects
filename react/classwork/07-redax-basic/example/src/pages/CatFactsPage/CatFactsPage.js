import React from 'react';
import CatFactsContainer from "../../components/CatFactsContainer";

const CatFactsPage = () => {

    return (
        <>
            <h1>Cat Facts</h1>
            <CatFactsContainer />
        </>
    )
}

export default CatFactsPage;