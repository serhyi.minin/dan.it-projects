import React from 'react';


const HomePage = () => (
        <>
            <h1>Awesome React + Redux counter</h1>
            <img src="https://i2.wp.com/gaprot.jp/wp-content/uploads/2017/12/20160713002241.jpg?fit=816%2C459&ssl=1" alt="React + Redux" height={200} />
            <h3>Count everything you can imagine</h3>
        </>
);


export default HomePage;