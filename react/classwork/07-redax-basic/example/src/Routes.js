import React from 'react';
import {Route, Switch} from "react-router-dom";
import HomePage from "./pages/HomePage";
import CounterPage from "./pages/CounterPage";
import CatFactsPage from "./pages/CatFactsPage";


const Routes = () => (
        <Switch>
            <Route exact path='/' component={HomePage}/>
            <Route exact path='/counter' component={CounterPage} />
            <Route exact path='/cat-facts' component={CatFactsPage} />
        </Switch>
);

Routes.propTypes = {};
Routes.defaultProps = {};

export default Routes;