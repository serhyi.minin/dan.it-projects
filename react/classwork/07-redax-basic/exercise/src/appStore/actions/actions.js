export const ADD_NOTE = 'ADD_NOTE';
export const TOGGLE_ISDONE = 'TOGGLE_ISDONE';
export const GET_DATA = 'GET_DATA';
export const SET_IS_OPEN_MODAL = 'SET_IS_OPEN_MODAL';
export const SET_CONFIG_MODAL = 'SET_CONFIG_MODAL';
export const REMOVE_NOTE = 'REMOVE_NOTE';