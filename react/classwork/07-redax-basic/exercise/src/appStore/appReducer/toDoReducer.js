import {ADD_NOTE, TOGGLE_ISDONE, GET_DATA, REMOVE_NOTE} from "../actions/actions";
const initialState = {
    items: []
};

export const toDoReducer = (state = initialState, action) =>{
    switch (action.type){
        case ADD_NOTE:
            return {...state, items: [...state.items, action.payload]};

        case TOGGLE_ISDONE:
            const idIndex = state.items.findIndex(({id}) => action.payload === id);

            if (idIndex === -1) return state;
            // const newState = { ...state };
            // newState.items[idIndex].isDone = !newState.items[idIndex].isDone;
            // return newState;

            const newItems = [...state.items]
            newItems[idIndex].isDone = !newItems[idIndex].isDone;
            return {...state, items: newItems};
        case GET_DATA:
            return {...state, items: action.payload};
        case REMOVE_NOTE:
            return {...state, items: action.payload};
        default:{
            return state
        }
    }
}