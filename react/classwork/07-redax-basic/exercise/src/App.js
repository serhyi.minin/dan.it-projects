import './App.css';
import AddNote from "./components/AddNote";
import NoteItem from "./components/NoteItem";
import NoteContainer from "./components/NoteContainer";
import { Provider } from "react-redux";
import {appStore} from "./appStore/appStore";
import Modal from "./components/Modal";

function App() {
  return (
      <Provider store={appStore}>
            <div className="App">
                <AddNote />
                <NoteContainer />
            </div>
          <Modal />
      </Provider>
  );
}

export default App;
