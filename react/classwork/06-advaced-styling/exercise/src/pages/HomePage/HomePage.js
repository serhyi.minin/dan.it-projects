import React from "react";
import Item from "../../components/Item";

const HomePage = (props) => {
    const {items, openModal} = props
    return (
        <div>
            <h1>HOME</h1>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {items.map(element =>{
                   return <Item openModal={openModal} key={element.id} color={element.color} name={element.name}
                          id={element.id} isFavorite={element.isFavorite}/>
                })}
            </div>
        </div>
    )
}

export default HomePage;