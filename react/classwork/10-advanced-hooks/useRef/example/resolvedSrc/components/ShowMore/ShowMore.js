import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import styles from './ShowMore.module.scss';


const ShowMore = (props) => {
    const {text} = props;
    const [isShowMore, setIsShowMore] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const height = isOpen ? 'auto' : '100px';

    const ref = useRef(null);

    useEffect(() => {
       const height = ref.current?.getBoundingClientRect()?.height;
       if (height && height > 100) {
           setIsShowMore(true);
       }
    }, [ref]);

    return (
        <div className={styles.root}>
            <div ref={ref} className={styles.textWrapper} style={isShowMore ? {height} : {}}>
                <p className={styles.textContainer}>{text}</p>
            </div>
            {isShowMore &&
            <button
                type='button'
                onClick={() => setIsOpen(prev => !prev)}
            >
                Show {isOpen ? 'Less' : 'More'}
            </button>
            }
        </div>
    )
}

ShowMore.propTypes = {
    text: PropTypes.string,
};
ShowMore.defaultProps = {
    text: '',
};

export default ShowMore;