import React from 'react';
import PropTypes from 'prop-types';
import styles from './User.module.scss';

const User = (props) => {
    const { name, avatar } = props;

    if (!name) return null;

    return (
        <div className={styles.user}>
            <h3>{name}</h3>
            <img src={avatar} alt={name} width={150} height={150}/>
        </div>
    )
}

User.propTypes = {
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string
};
User.defaultProps = {
    avatar: null,
};

export default User;