import React from "react";
import './App.scss';
import Header from "./components/Header";
import GoToTop from "./components/GoToTop/GoToTop";
import Spacer from "./components/Spacer";
import useInView from "./hooks/useInView";

function App() {
    const [ref, inView] = useInView();
    console.log('inView', inView);

    return (
        <div className="App">
            <Header />
            <Spacer />
            <h1 ref={ref}>HELLO!!!</h1>
            {inView && <img src={"https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg"} alt=""/>}
            <Spacer />
            <GoToTop />
        </div>
    );
}

export default App;
