import React from 'react';
import styles from './GoToTop.module.scss';

const GoToTop = () => {
    return (
        <>
            <button onClick={() => {}} className={styles.root}>UP</button>
        </>
    )
}

export default GoToTop;