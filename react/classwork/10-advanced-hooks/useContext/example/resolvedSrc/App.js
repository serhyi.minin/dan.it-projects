import './App.scss';
import Header from "./components/Header";
import Routes from "./Routes";
import {BrowserRouter} from "react-router-dom";
import darkTheme from './styles/dark.module.scss';
import lightTheme from './styles/light.module.scss';
import {useState} from "react";
import classNames from "classnames";
import AppContextProvider from "./context/AppContextProvider";

function App() {
    const [isLightTheme, setIsLightTheme] = useState(true);
    const themeStyles = isLightTheme ? lightTheme : darkTheme;

    const contextValue = {
        themeStyles,
        isLightTheme,
        setIsLightTheme,
    }

    return (
        <BrowserRouter>
            <AppContextProvider value={contextValue}>
                <div className={classNames("App", themeStyles.theme)}>
                    <Header title="React Context"/>
                    <Routes/>
                </div>
            </AppContextProvider>
        </BrowserRouter>
    );
}

export default App;
