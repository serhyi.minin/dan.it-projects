import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import styles from "./PostsContainer.module.scss";

class Post extends PureComponent {
  render() {
    const { userId, title, body } = this.props;

    return (
      <div className={styles.root}>
        <span>User: {userId}</span>
        <h3>{title}</h3>
        <p>{body}</p>
      </div>
    );
  }
}

Post.propTypes = {
  body: PropTypes.any,
  title: PropTypes.any,
  userId: PropTypes.any,
};

export default Post;
