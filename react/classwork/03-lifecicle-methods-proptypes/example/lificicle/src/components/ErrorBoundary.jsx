import React from "react";
import SomethingWentWrong from "./SomethingWentWrong/SomethingWentWrong";

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Оновлюємо стан, щоб наступний рендер показав запасний UI.
    return { hasError: true };
  }

  //   componentDidCatch(error, errorInfo) {
  //     // Ви також можете передати помилку в службу звітування про помилки
  //     logErrorToMyService(error, errorInfo);
  //   }

  render() {
    const { hasError } = this.state;

    if (hasError) {
      // Ви можете відрендерити будь-який власний запасний UI
      return <SomethingWentWrong />;
    }

    return this.props.children;
  }
}
export default ErrorBoundary;
