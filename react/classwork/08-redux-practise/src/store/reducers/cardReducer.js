 import {INIT_CARDS, SET_IS_LOADING_CARDS, TOGGLE_IS_FAVOURITE_CARDS} from "../actions";


 const initialState = {
    data:[],
     isLoading:true
 }

const cardReducer = (state = initialState , {type, payload}) => {
switch (type) {
    case INIT_CARDS: {
        return {...state, data: payload}
    }
    case SET_IS_LOADING_CARDS: {
        return {...state, isLoading: payload}
    }
    case TOGGLE_IS_FAVOURITE_CARDS:
        const newData = [...state.data];
        const targetIndex = newData.findIndex(e => e.id === payload.id);
        newData[targetIndex] = payload;
        return {...state, data: newData};


    default: { return state}


}
}

 export default cardReducer