import React from "react";
import styles from "./HeaderNav.module.scss"
import {Link, NavLink} from "react-router-dom";


const HeaderNav = () => {
    return(
        <nav>
            <ul>
                <li>
                    <NavLink to="/cards" activeClassName={styles.activeLink}>Cards</NavLink>
                </li>
                <li>
                    <NavLink to="/cart" activeClassName={styles.activeLink}>Cart</NavLink>
                </li>
                <li>
                    <NavLink to="/favourite" activeClassName={styles.activeLink}>Favourite</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default HeaderNav