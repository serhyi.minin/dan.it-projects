import React, {Component, PureComponent} from "react";
import styles from "./Main.module.scss"

class Main extends Component{

    render() {
        const { emails, readEmail } = this.props
        return(
            <>
                {emails.map(({from, topic, body, id, isRead}) =>
                    <div className={`${styles.emailContainer} ${isRead ? styles.isRead : ''}`} key={id} onClick={() => readEmail(id)}>
                        <span>{from}</span>
                        <h3>{topic}</h3>
                        <p>{body}</p>
                        {isRead ? <p>read</p> : <p>not read</p>}
                    </div>
                )}
            </>


        )
    }
}

export default Main


