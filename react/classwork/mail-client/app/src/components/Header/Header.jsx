import React, { Component } from "react";
import style from "./Header.module.scss";
import user from "./img/user.png";

const Header = (props) => {
  const { age, name, title, countMessage } = props;
  return (
    <header className={style.header}>
      <div className={style.headerTop}>
        <h1 className={style.headerTitle}>{title}</h1>
        <div className={style.headerMain}>
          <span>Emails: {countMessage}</span>
          <div className={style.headerProfile}>
            <img width="55" src={user} alt="user" />
            <span>{name}</span>
            <span>{age}</span>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
