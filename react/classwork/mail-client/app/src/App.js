import React, { useState } from "react";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Main from "./components/Main/Main";

const App = (props) => {
  const [title, setTitle] = useState("some title");
  const [inputValue, setInputValue] = useState("");

  const updateTitle = () => {
    setTitle((current) => ({
      ...current,
      title: current.inputValue,
      inputValue: "",
    }));

    /* state = {

        inputValue: '',
        user:{
            name: "Marina",
            age: 25,
            hobby: "gym"
        },
        emails: [
            {
                id: 1,
                isRead: false,
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.'
            },
            {
                id: 2,
                isRead: false,
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.'
            },
            {
                id: 3,
                isRead: false,
                from: 'Natalia Pemchyshyn',
                topic: 'JavaScript Developer at GlobalNogic',
                body: 'Привіт!Я рекрутер компанії GlobalNogic. Ми зараз у пошуках Lead/Senior React Developer (фултайм/ремоут) на фармацевтичний проект. Розробляємо девайс, який моніторить стан здоров\'я свійських тварин.Стек: React, Ionic, HTML, CSS, RxJs, Java. Поспілкуємось?'
            },
        ]

    }

    incrAge = () => {
        this.setState(current => (
            {...current,
                user: {
                    ...current.user,
                    age: current.user.age + 1
                }
            }));
    }

    updateTitle = () => {
        this.setState(current => (
            {...current,
                title: current.inputValue,
                inputValue: '',
            }));
    }

    handleChange = ( { target: { value } } ) => {
        this.setState(current => ({
            ...current,
            inputValue: value,
        }))
    }

    readEmail = (id) => {
        this.setState(current => {
            const index = current.emails.findIndex(({id: elemId}) => {
                return id === elemId
            })

            const newState = {...current}
            newState.emails[index].isRead = true
            return newState
        })
    } */

    const { user, emails } = this.state;

    return (
      <div className={styles.App}>
        <Header {...this.state} />

        <button onClick={this.incrAge}>Incr age</button>
        <button
          onClick={() => {
            this.setState((current) => ({
              ...current,
              emails: [...current.emails, "dhfkshdfks"],
            }));
          }}
        >
          Push Email
        </button>
        <Main emails={emails} readEmail={this.readEmail}></Main>
        <Footer title={title} countMessage={emails.length} />
      </div>
    );
  };
};

export default App;
