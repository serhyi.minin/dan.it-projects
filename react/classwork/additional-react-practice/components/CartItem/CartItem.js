import React from 'react';
import PropTypes from 'prop-types';
import styles from './CartItem.module.scss';
import {Button} from "@mui/material";

const CartItem = () => {

    return (
        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.itemAvatar} src="https://cdn.27.ua/499/06/50/1115728_24.jpeg" alt="Шуруповерт аккумуляторный CD-37-12 "/>
                </div>
                <span className={styles.title}>Шуруповерт аккумуляторный CD-37-12 </span>
            </div>


            <span className={styles.quantity}>12</span>

            <div className={styles.btnContainer}>
                <Button className={styles.btn} variant="contained">OK</Button>
                <Button className={styles.btn} variant="contained">OK</Button>
                <Button className={styles.btn} variant="contained">OK</Button>
            </div>

        </div>
    )
}

CartItem.propTypes = {};
CartItem.defaultProps = {};

export default CartItem;