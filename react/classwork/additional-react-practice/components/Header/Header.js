import React from 'react';
import styles from './Header.module.scss';

const Header = () => {

    return (
        <header className={styles.root}>
            {/* Left container */}
            <div>
                <span>Something</span>
            </div>

            {/* Centered navigation */}
            <nav>
                <ul>
                    <li>
                        <a href="">Link</a>
                    </li>
                    <li>
                        <a href="">Link</a>
                    </li>
                </ul>
            </nav>

            {/* Right container */}
            <ul>
                <li>
                    <a href="">Link</a>
                </li>

                <li>
                    <a href="">Link</a>
                </li>
            </ul>
        </header>
    );
};

export default Header;
