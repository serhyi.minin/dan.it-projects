import './App.css';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';


function App() {
  return (
      <Router>
        <div className="App">
          <h1>asdfsdfsdf</h1>

            <nav>
                <ul>
                    <li>
                        <Link to="/" className="link">Home</Link>
                    </li>

                    <li>
                        <Link to="/blog">Blog</Link>
                    </li>

                    <li>
                        <Link to="/contacts">Contacts</Link>
                    </li>
                </ul>
            </nav>

            <Switch>

                <Route exact path="/">
                    <h1>Home</h1>
                </Route>

                <Route path="/blog/123">
                    <h1>Blog 123</h1>
                </Route>

                <Route path="/blog">
                    <h1>Blog</h1>
                </Route>


                <Route path="/contacts">
                    <h1>Contacts</h1>
                </Route>

            </Switch>


        </div>
      </Router>
  );
}

export default App;
