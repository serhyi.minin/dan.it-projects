import React from 'react';
import { CircularProgress } from "@mui/material"; // https://mui.com/api/circular-progress/


const Preloader = (props) => {
        const { color, size } = props;

        return <CircularProgress color={color} size={size} />;
}


export default Preloader;
