import React from "react";
import {Switch, Route} from 'react-router-dom'
import HomePage from "../pages/HomePage";
import BlogPage from "../pages/BlogPage";
import PostPage from "../pages/PostPage";

function Routes(){
    return(
        <Switch>
            <Route exact path="/posts" component={BlogPage} />
            <Route exact path="/posts/:id" component={PostPage} />
            <Route exact path="/" component={HomePage} />
        </Switch>
    );
}

export default Routes