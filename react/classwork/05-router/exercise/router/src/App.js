import React, {useEffect, useState} from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import PostsContainer from "./components/PostsContainer";
import Routes from "./Routes/Routes";
import { BrowserRouter } from "react-router-dom";
import './App.scss';

const headersProps = {
    title: 123,
    user: { name: 'Sam', age: 26,  avatar: 'https://i.pravatar.cc/40'},
};


function App()  {

        return (
            <BrowserRouter>

                <div className="App">
                    <Header title={headersProps.title} user={headersProps.user}  />
                    <Routes/>

                    <Footer title="PropTypes" year={new Date().getFullYear()} onOrderFunc={() => console.log('Order call')} />

                </div>

            </BrowserRouter>
        );

}

export default App;
