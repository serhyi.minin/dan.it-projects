class Modal {
  constructor() {
    this.container = document.createElement("div");
    this.background = document.createElement("div");
    this.mainContainer = document.createElement("div");
    this.closeButton = document.createElement("button");
    this.contentWrapper = document.createElement("div");
    this.buttonWrapper = document.createElement("div");
  }

  createElements() {
    this.container.classList.add("modal");
    this.background.classList.add("modal__background");
    this.mainContainer.classList.add("modal__main-container");
    this.closeButton.classList.add("modal__close");
    this.contentWrapper.classList.add("modal__content-wrapper");
    this.buttonWrapper.classList.add("modal__button-wrapper");

    this.mainContainer.append(this.closeButton);
    this.mainContainer.append(this.contentWrapper);
    this.mainContainer.append(this.buttonWrapper);

    this.container.append(this.mainContainer);
    this.container.append(this.background);

    this.closeButton.addEventListener("click", this.closeMe.bind(this));
  }
  closeMe() {
    this.container.remove();
  }

  render(selector = "body") {
    this.createElements();
    document.querySelector(selector).append(this.container);
  }
}

export class DeleteModal extends Modal {
  constructor(titlePost, deleteFunction) {
    super();
    this.titlePost = titlePost;
    this.deleteFunction = deleteFunction;
    this.titleElement = document.createElement("h3");
    this.buttonYes = document.createElement("button");
    this.buttonNo = document.createElement("button");
  }

  createElements() {
    super.createElements();
    this.titlePost.innerHTML = `Do you really want to delete "${this.titlePost}"?`;

    this.buttonYes.innerHTML = "Подтвердить";
    this.buttonYes.classList.add("modal__confirm-btn");
    this.buttonYes.addEventListener(`click`, this.deleteFunction);

    this.buttonNo.innerHTML = "Отменить";
    this.buttonNo.classList.add("modal__cancel-btn");
    this.buttonNo.addEventListener(`click`, this.closeMe.bind(this));

    this.contentWrapper.append(this.titleElement);
    this.buttonWrapper.append(this.buttonYes);
    this.buttonWrapper.append(this.buttonNo);
  }
}

//Tests
new Modal().render();
