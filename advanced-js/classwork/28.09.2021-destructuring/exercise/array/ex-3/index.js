/*
Напишите функцию, которая создает объект.
В качестве аргументов она принимает в себя имя, фамилию, и перечень строк формата "имяСвоства: значение". Их может быть много.

Пример работы:

const user = createObject("Jhon", "Johnson", "age: 31", "wife: Marta", "dog: Bob");
=>
user = {
    name: "Jhon",
    lastName: "Johnson",
    age: "31",
    wife: "Marta",
    dog: "Bob",
}
 */

const createObject = () => {};

createObject("Shanon", "Kerr", "age: 24", "cat: Honey", "city: LA")
createObject("Kaison", "Dalby")
createObject("Rebekka", "Solomon", "country: France", "city: Marseilles");

