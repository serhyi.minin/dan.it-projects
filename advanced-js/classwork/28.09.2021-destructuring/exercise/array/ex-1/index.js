/*
Напишите код, который создает переменные с именем человека, а также с его профессией, выводит запись типа "userName is userProfession";
 */

const user1 = "Cristian Stuart; developer";
const user2 = "Archibald Robbins; seaman";
const user3 = "Zach Dunlap; lion hunter";
const user4 = "Uwais Johnston; circus artist";

/**
 *
 * @param {string} user
 */
const userLog = (user) => {
  const [userName, userProfession] = user.split(`;`);
  //   [userName, userProfession] = userArray;
  console.log(`${userName} is ${userProfession}`);
};

userLog(user1);
userLog(user2);
userLog(user3);
userLog(user4);
