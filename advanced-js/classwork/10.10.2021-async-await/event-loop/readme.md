# [Event-loop](https://bool.dev/blog/detail/obyasnenie-event-loop-v-javascript-s-pomoshchyu-vizualizatsii)

[Async JS](assets/async-js.png)

```js

const stack = [
    () => console.log(1),
    () => console.log(2),
    () => console.log(3),
    () => console.log(4),
    () => console.log(5),
    () => console.log(6),
    () => console.log(7),
    () => console.log(8),
    () => console.log(9),
    () => console.log(10),
    () => console.log(11),
    () => console.log(12),
    () => console.log(13),
    () => console.log(14),
    () => console.log(15),
];

const doTask = () => {
    if (stack[0] && typeof stack[0] === 'function'){
        stack[0]();
        stack.shift();
    }
}

setInterval(doTask, 100);

```
