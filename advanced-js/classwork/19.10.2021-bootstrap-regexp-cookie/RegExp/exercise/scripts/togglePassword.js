/**
 * @desc Find all input with type=password and add event listener to '.icon'.
 * onClick change input type and icon img;
 */
const setTogglePassword = () => {
    const inputsPasswordCollection = document.querySelectorAll('input[type="password"]');
    inputsPasswordCollection.forEach(input => {
        const container = input.parentNode;
        const icon = container?.querySelector('.icon')

        icon.addEventListener("click", () =>{
            if (input.type === "text") {
                input.type = "password";
                container.querySelector("img").src = "./svg/eye.svg"
            } else {
                input.type = "text"
                container.querySelector("img").src = "svg/eye-off.svg"
            }
        })
    })
};

export default setTogglePassword;

