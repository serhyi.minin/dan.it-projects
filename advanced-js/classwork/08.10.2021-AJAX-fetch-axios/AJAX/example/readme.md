# AJAX

Asynchronous Javascript and XML. На самом деле, AJAX не является новой технологией, так как и Javascript, и XML существуют уже довольно продолжительное время, а AJAX — это синтез обозначенных технологий.

AJAX использует два метода работы с веб-страницей: изменение Web-страницы не перезагружая её, и динамическое обращение к серверу. Второе может осуществляться несколькими способами, в частности, XMLHttpRequest.

# [XMLHttpRequest](https://developer.mozilla.org/ru/docs/Web/API/XMLHttpRequest)

`XMLHttpRequest` это API, который предоставляет клиенту функциональность для обмена данными между клиентом и сервером. Данный API предоставляет простой способ получения данных по ссылке без перезагрузки страницы. Это позволяет обновлять только часть веб-страницы не прерывая пользователя.

```js

// 1. Создаем XMLHttpRequest
const xhr = new XMLHttpRequest();

// 2. Инициализируем запрос
xhr.open(method, URL);

// 3. Послать запрос.
xhr.send(body);

// 4. Слушать события на xhr, чтобы получить ответ.

```

## Три наиболее используемых события:

* `load` – происходит, когда получен какой-либо ответ, включая ответы с HTTP-ошибкой, например 404.
* `error` – когда запрос не может быть выполнен, например, нет соединения или невалидный URL.
* `progress `– происходит периодически во время загрузки ответа, сообщает о прогрессе.


```js

xhr.onload = () => {
    console.log(xhr);
    if (xhr.status === 200) {
        console.log(JSON.stringify(xhr.response))
    }
};

xhr.onprogress = (event) => {
    // event.loaded - количество загруженных байт
    // event.lengthComputable = равно true, если сервер присылает заголовок Content-Length
    // event.total - количество байт всего (только если lengthComputable равно true)
    if (event.lengthComputable) {
        console.log(`Получено ${event.loaded} из ${event.total} байт`);
    } else {
        console.log(`Получено ${event.loaded} байт`); // если в ответе нет заголовка Content-Length
    }
};

xhr.onerror = function() {
    console.error('Something goes wrong');
};

```

Также есть возможность указать формат ожидаемого ответа

```js

xhr.responseType = 'json';

```

<hr />

```js

// 1. Создаем XMLHttpRequest
const xhr = new XMLHttpRequest();

// 2. Инициализируем запрос
xhr.open('GET', 'https://catfact.ninja/fact');

// 3. Послать запрос.
xhr.send();


xhr.onload = () => {
    console.log(xhr);
    if (xhr.status === 200) {
        console.log(JSON.parse(xhr.response));
    } else {
        console.error(`Bad request! Status: ${xhr.status}`);
    }
};

xhr.onprogress = (event) => {
    // event.loaded - количество загруженных байт
    // event.lengthComputable = равно true, если сервер присылает заголовок Content-Length
    // event.total - количество байт всего (только если lengthComputable равно true)
    console.log(`Загружено ${event.loaded} из ${event.total}`);
};

xhr.onerror = function() {
    console.error('Something goes wrong');
};

```
