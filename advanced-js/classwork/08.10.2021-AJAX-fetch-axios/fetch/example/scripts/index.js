fetch('https://ajax.test-danit.com/api/json/users', {
    method: 'POST',
    body: JSON.stringify({
        name: 'John'
    }),
    headers: {
        'Content-Type': 'application/json'
    }
})
    .then(({ headers }) => {
        console.log(headers);
        console.log(headers.get('content-type'));
    });
