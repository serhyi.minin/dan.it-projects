# Promise
Объект Promise используется для отложенных и асинхронных вычислений.

```js

new Promise(executor);
new Promise(function(resolve, reject) {});

```

`executor`(исполнитель) - функция, которая передается в `Promise`. Когда `Promise` создаётся, она запускается автоматически. 

Аргументы `resolve` и `reject` - это колбэки, которые предоставляет сам JavaScript. Наш код – только в теле исполнителя (внутри самой функции).

Когда `Promise` получает результат, сейчас или позже – не важно, он должен вызвать один из этих колбэков:
* `resolve(value)` — если работа завершилась успешно, с результатом `value`;
* `reject(error)` — если произошла ошибка, `error` – объект ошибки.


Итак, исполнитель запускается автоматически, он должен выполнить работу, а затем вызвать `resolve` или `reject`.

```js

const myFirstPromise = new Promise((resolve, reject) => {
    // resolve('Some Value');
    // reject(new Error('Some Error'));
})

console.log(myFirstPromise)

```
<hr />

## Состояния Promise
`state`(состояние) - внутренние свойства объекта `Promise`:

* `pending`(ожидание) - начальное состояние, не исполнен и не отклонён.
* `fulfilled` (исполнено) - операция завершена успешно (при вызове `resolve`).
* `rejected` (отклонено): операция завершена с ошибкой (при вызове `reject`).

При создании промис находится в ожидании (`pending`), а затем может стать исполненным (`fulfilled`), вернув полученный результат (значение), или отклонённым (`rejected`), вернув причину отказа.

![](assets/promise_1.png)
<hr />

## Then, catch, finally

`then` - метод `Promise` используемый для обработки результата.

`then` принимает два аргумента - callback функции `onFulfilled` и `onRejected`;

```js

p.then(value => {
  // выполнение
  }, reason => {
  // отклонение
});

```

```js

const onResolve = (value) => {};
const onReject = (error) => {};

new Promise((resolve, reject) => {}).then(onResolve, onReject)

```

```js

const myFirstPromise = new Promise((resolve, reject) => {
    setTimeout(() => resolve('Success'), 2000);
    setTimeout(() => reject(new Error('Error')), 3000);
})

myFirstPromise.then(() => {}, () => {})

```

### Важно!

<blockquote>
Метод <code>then</code> возвращает <code>Promise</code>. Это позволяет вызывать цепочки <code>then</code>.
</blockquote>

```js

const jsonString = JSON.stringify([1, 2, 3]);

const promise = new Promise(resolve => setTimeout(() => resolve(jsonString), 3000));

promise.then(value => {
    console.log('First then: ', value);
    return JSON.parse(value);
})
    .then(value => console.log('Second then: ', value))

```

### Catch

Метод `catch` возвращает `Promise` и работает только в случае отклонения `Promise`. Ведёт себя аналогично вызову `Promise.then(undefined, onRejected)`.

```js

p.catch(function(reason) {
   // отказ
});

```

```js

const promise = new Promise((resolve, reject) => {
    setTimeout(() => resolve('Success'), 2000);
    setTimeout(() => reject(new Error('Error')), 3000);
})

myFirstPromise.then(() => {}).catch(() => {});

```

### Finally

Метод `finally` возвращает `Promise`. Когда `Promise` был выполнен, вне зависимости успешно или с ошибкой, указанная функция будет выполнена. Это даёт возможность запустить один раз определённый участок кода, который должен выполниться вне зависимости от того, с каким результатом выполнился `Promise`.

```js

p.finally(function() {
   // завершён (успешно или с ошибкой)
});

```

```js

const promise = new Promise((resolve, reject) => {
setTimeout(() => resolve('Success'), 2000);
setTimeout(() => reject(new Error('Error')), 3000);
})

myFirstPromise.then(() => {}).catch(() => {}).finally(() => {});

```

<hr />

## Callback hell

```js

const getLink = (url, callback) => {
    setTimeout(() => {
        callback(url);
        console.log(url);
    }, 2000)
}

const getFirstLink = (url, callback) => {
    callback(url);
}

getFirstLink('hhtps://www.some-url-1.com', (url) => {
    if (typeof url === 'string') {
        getLink('hhtps://www.some-url-2.com', (url) => {
            if (url.length > 1) {
                getLink('hhtps://www.some-url-3.com', (url) => {
                    if (url.includes('www.some-url')) {
                        getLink('hhtps://www.some-url-4.com', (url) => {
                            if (2 > 1) {
                                getLink('hhtps://www.some-url-4.com', () => console.log('done'))
                            }
                        })
                    }
                })
            }
        })
    }
})

```
<hr />

## Short view to fetch

Глобальный метод `fetch` позволяет легко и логично получать ресурсы по сети асинхронно. 

`fetch` принимает в качестве аргумента ссылку на ресурс (это не все аргументы, но подоробнее мы рассмотрим эту тему на следующем занятии) и возвращает нам `Promise`.

<blockquote>
Самый простой способ использования <code>fetch()</code> заключается в вызове этой функии с одним аргументом — строкой, содержащей путь к ресурсу, который вы хотите получить — которая возвращает <code>Promise</code>, содержащее ответ (объект <code>Response</code>).
<br>
<br>
Конечно, это просто HTTP-ответ, а не фактический JSON. Чтобы извлечь содержимое тела JSON из ответа, мы используем метод <code>json()</code>;
</blockquote>

```js

fetch('https://catfact.ninja/fact')
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => {
        throw error
    });

```


# Методы `Promise`

## [Promise.all](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise/all)

1. Принимает в качестве аргументов массив `Promise` (перечисляемый аргумент `Promise`)

2. Ожидает исполнения всех промисов или отклонения любого из них.

3. Возвращает промис, который исполнится после исполнения всех `Promise` в массиве. В случае, если любой из `Promise` будет отклонён, `Promise.all` будет также отклонён.

4. Исполненный `Promise` вернет нам массив всех значение из массива `Promise`

```js

const promise1 = new Promise(resolve => {
    setTimeout(() => resolve('Hello from Promise'), 3000);
})

const promise2 = Promise.resolve(21);

const promise3 = new Promise((resolve, reject) => {
    fetch('https://catfact.ninja/fact')
        .then(response => response.json())
        .then(data => resolve(data))
        .catch(error => reject(error));
})

Promise.all([promise1, promise2, promise3]).then(data => console.log(data));

```

## [Promise.allSettled](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise/allSettled)

То же самое что и `Promise.all`, возвращает `Promise`, который исполняется когда все полученные `Promise` завершены (исполнены или отклонены), содержащий массив результатов исполнения полученных `Promise`.


## [Promise.race](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise/race)

Метод `Promise.race(iterable)` возвращает выполненный или отклонённый `Promise`, в зависимости от того, с каким результатом завершится первый из переданных `Promise`, со значением или причиной отклонения этого `Promise`.


```js

const promise1 = new Promise(resolve => {
    setTimeout(() => resolve('Hello from Promise'), 3000);
})

const promise2 = new Promise(_, reject => {
    setTimeout(() => reject(new Error('Ooooops, something have come up')), 4000);
})

Promise.all([promise1, promise2]).then(data => console.log(data));

```
