# [HTTP Заголовки](https://developer.mozilla.org/ru/docs/Web/HTTP/Headers)

```js

fetch('https://ajax.test-danit.com/api/json/posts', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer BXhbkzhbkabJBX&12312vJHVASJhvjh1j231j231j23j123j12j3h',
    },
    body: JSON.stringify({
        key: 'value',
    }),
}).then(res => res.json()).then(data => console.log(data));

```

```js

axios.post('https://ajax.test-danit.com/api/json/posts', { key: 'value' }, {
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer BXhbkzhbkabJBX&12312vJHVASJhvjh1j231j231j23j123j12j3h',
    },
}).then(data => console.log(data));

```
