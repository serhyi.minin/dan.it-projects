# [Методы HTTP запроса](https://developer.mozilla.org/ru/docs/Web/HTTP/Methods)
[Dan.it API](https://ajax.test-danit.com/api-pages)

<blockquote>
HTTP определяет множество методов запроса, которые указывают, какое желаемое действие выполнится для данного ресурса. Несмотря на то, что их названия могут быть существительными, эти методы запроса иногда называются HTTP глаголами.
</blockquote>


## GET
Метод GET запрашивает представление ресурса. Запросы с использованием этого метода могут только извлекать данные.

```js

fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json()).then(data => console.log(data));

```

```js

axios.get('https://ajax.test-danit.com/api/json/posts').then(data => console.log(data));

```

## POST
POST используется для отправки сущностей к определённому ресурсу. Часто вызывает изменение состояния или какие-то побочные эффекты на сервере.

```js

fetch('https://ajax.test-danit.com/api/json/posts', {
    method: 'POST',
    body: {
        title: 'My post',
        body: 'something very interesting',
    }
}).then(res => res.json()).then(data => console.log(data));

```

```js

axios.post('https://ajax.test-danit.com/api/json/posts', {
    title: 'My post',
    body: 'something very interesting',
}).then(data => console.log(data));

```

## PUT
PUT заменяет все текущие представления ресурса данными запроса.

```js

fetch('https://ajax.test-danit.com/api/json/posts/99', {
    method: 'PUT',
    body: JSON.stringify({
        title: 'My edited post',
        body: 'something else, but still very interesting',
    })
}).then(res => res.json()).then(data => console.log(data));


```

```js

axios.put('https://ajax.test-danit.com/api/json/posts/1', {
    title: 'My edited post',
    body: 'something else, but still very interesting',
}).then(data => console.log(data));

```

## PATCH
PATCH используется для частичного изменения ресурса.

```js

fetch('https://ajax.test-danit.com/api/json/posts/99', {
    method: 'PATCH',
    body: JSON.stringify({
        title: 'My edited post only',
    })
}).then(res => res.json()).then(data => console.log(data));


```

```js

axios.patch('https://ajax.test-danit.com/api/json/posts/1', {
    title: 'My edited post only',
}).then(data => console.log(data));

```

## DELETE
DELETE удаляет указанный ресурс.

```js

fetch('https://ajax.test-danit.com/api/json/posts/99', {
    method: 'DELETE',
}).then(res => res.json()).then(data => console.log(data));

```

```js

axios.delete('https://ajax.test-danit.com/api/json/posts/1').then(data => console.log(data));

```
