const setToDo = require('./setToDo');
const deleteToDo = require('./deleteToDo');

module.exports = (app) => {
    setToDo(app);
    deleteToDo(app);
}
