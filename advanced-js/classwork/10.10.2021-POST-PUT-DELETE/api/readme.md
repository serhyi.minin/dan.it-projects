#API

# Register

## `POST: http://localhost:3000/register`
Принимает в `body` данные пользователя, в случае успеха возвращает `JSON` с сообщением об успешной регистрации и объектом созданного пользователя. 

#### request.body:
```js

body: {
    name: {string},
    age: {string},
    city: {string},
    email: {string}.required,
    password: {string}.required,
    repeatPassword: {string}.required,
}

```

#### response:
Success:
```js
status: 200;

{
    status: 'success',
    message: {string},
    user: {
        id: {number}
        name: {string},
        age: {string},
        city: {string},
        email: {string},
        avatar: {string},
        toDoList: {array},
    },
}
```
Wrong 'Content-Type' header:
```js

status: 418;

{
    status: `error`,
    error: {string},
}
```

Wrong body:
```js
status: 400;

{
    status: `error`,
    error: {string},
}
```
<hr />

# Log-in

## `POST: http://localhost:3000/log-in`
Принимает в `body` email и пароль пользователя, в случае успеха возвращает `JSON` с сообщением об успешной авторизации, объектом созданного пользователя и токеном авторизации. 

#### request.body:
```js

body: {
    email: {string}.required,
    password: {string}.required,
}

```

#### response:
Success:
```js
status: 200;

{
    status: 'success',
    data: {
        id: {number}
        name: {string},
        age: {string},
        city: {string},
        email: {string},
        avatar: {string},
        toDoList: {array},
    },
    authToken: {string},
}
```

Wrong 'Content-Type' header:
```js

status: 418;

{
    status: `error`,
    error: {string},
}
```
Wrong body:
```js
status: 400;

{
    status: `error`,
    error: {string},
}
```

User not found:
```js
status: 404;

{
    status: `error`,
    error: {string},
}
```

Invalid password:
```js
status: 401;

{
    status: `error`,
    error: {string},
}
```
<hr />

# ToDo

## `POST: http://localhost:3000/to-do`
Принимает в `body` данные пользователя и текст нового ToDo, в случае успеха возвращает `JSON` с сообщением об успешном добавлении и объектом обновленного пользователя. Требует токена авторизации в заголовке.

#### Auth header:
```js
headers = {
    "Authorization": "Token YWxhZGRpbjpvcGVuc2VzYW1lYWxhZGRpbjpvcGVuc2VzYW1lYWxhZGRpbjpvcGVuc2VzYW1lYWxhZGRpbjpvcGVuc2VzYW1lYWxhZGRpbjpvcGVuc2VzYW1l"
}
```

#### request.body:
```js

body: {
    userId: {string||number}.required,
    toDo: {string}.required,
}

```

#### response:
Success:
```js
status: 200;

{
    status: 'success',
    user: {
        id: {number}
        name: {string},
        age: {string},
        city: {string},
        email: {string},
        avatar: {string},
        toDoList: [{
            id: {string},
            text: {string},
        }],
    },
}
```

Wrong 'Content-Type' header:
```js

status: 418;

{
    status: `error`,
    error: {string},
}
```


Wrong 'Authorization' header:
```js

status: 401;

{
    status: `error`,
    error: {string},
}
```
Wrong body:
```js
status: 400;

{
    status: `error`,
    error: {string},
}
```
User not found:
```js
status: 404;

{
    status: `error`,
    error: {string},
}
```


# ToDo

## `DELETE: http://localhost:3000/to-do?toDoId={string}&userId={number||string}&`

Принимает в [query параметрах](https://howto.caspio.com/parameters/parameters-as-query-string-values/) id пользователя `userId` и id ToDo `toDoId`, в случае успеха возвращает `JSON` с сообщением об успешном удалении и объектом обновленного пользователя. Требует токена авторизации в заголовке.

#### Auth header:
```js
headers = {
    "Authorization": "Token YWxhZGRpbjpvcGVuc2VzYW1lYWxhZGRpbjpvcGVuc2VzYW1lYWxhZGRpbjpvcGVuc2VzYW1lYWxhZGRpbjpvcGVuc2VzYW1lYWxhZGRpbjpvcGVuc2VzYW1l"
}
```

#### response:
Success:
```js
status: 200;

{
    status: 'success',
    user: {
        id: {number}
        name: {string},
        age: {string},
        city: {string},
        email: {string},
        avatar: {string},
        toDoList: [{
            id: {string},
            text: {string},
        }],
    },
}
```
Wrong 'Authorization' header:
```js

status: 401;

{
    status: `error`,
    error: {string},
}
```
Wrong query params:
```js
status: 400;

{
    status: `error`,
    error: {string},
}
```
User or toDo not found:
```js
status: 404;

{
    status: `error`,
    error: {string},
}
```
