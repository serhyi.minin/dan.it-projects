document.querySelector(`.form`).addEventListener(`submit`, (e) => {
  e.preventDefault();
  const body = {};
  document.querySelectorAll(`input`).forEach(({ name, value }) => {
    body[name] = value;
  });

  axios
    .post(`http://localhost:3000/register`, body, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then(({ data, status }) => {
      if (status === 200) {
        location.href = location.href.split(`/sign-up`)[0] + `/sign-in`;
        alert(data.message);
      }
    })
    .catch(({ response }) => {
      //axios have response
      alert(response.data.error);
    });
});
