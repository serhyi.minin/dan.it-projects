const usersURL = `https://ajax.test-danit.com/api/json/users`;
const postsURL = `https://ajax.test-danit.com/api/json/posts`;
const delPostURL = `http://ajax.test-danit.com/api/json/posts/`;
class Card {
  constructor(id, name, email, title, body) {
    this.postId = id;
    this.name = name;
    this.email = email;
    this.title = title;
    this.message = body;
    this.tweetBox = document.querySelector(".tweetBox");
    // this.currentPost = "";
    this.postDelete = document.createElement("div");
  }

  createPost() {
    this.content = `
    <div class="post" data-postid=${this.postId}>
    <div class="post__avatar">
       <a href="#"><img src="./src/img/avatar.png" alt="Person"></a>
    </div>
    <div class="post__body">
       <div class="post__header">

        <div class="post__box">
              <div class="post__wrapper">
                  <a href="#">
                    <h3 class="post__name"  data-userid=${this.name}>${this.name}</h3>
                  </a>
                  <a href="#"><img class="img-verified" src="./src/assets/verified.svg" alt="Verified">${this.email}</a>
              </div>

              <a class="post__more" href="#">
                  <svg class="img-more" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" version="1.1" fill: rgb(83, 100, 113)>
                  <circle cx="5" cy="12" r="2"></circle>
                  <circle cx="12" cy="12" r="2"></circle>
                  <circle cx="19" cy="12" r="2"></circle>
                  </svg>
              </a>
              <div class="post__delete">Delete</div>

          </div>

          <h4 class="post__title">${this.title}</h4>
          <p class="post__message">${this.message}</p>
       </div>
       <img class="post__img" src="./src/img/tweet-img.jpg" alt="">
       <div class="post__footer">
          <a href="#"><img class="img-reply" src="./src/assets/reply.svg" alt="Reply">169</a>
          <a href="#"><img class="img-retweet" src="./src/assets/retweet.svg" alt="Retweet">69</a>
          <a href="#"><img class="img-like" src="./src/assets/like.svg" alt="Like">79</a>
          <a href="#"><img class="img-share" src="./src/assets/share.svg" alt="Share"></a>
       </div>
    </div>
 </div>
`;
  }

  render() {
    this.createPost();
    this.tweetBox.insertAdjacentHTML(`afterend`, this.content);

    const threeDotsIcon = document.querySelector(".post__more");

    threeDotsIcon.addEventListener("click", (event) => {
      const delBtn = event.target.nextElementSibling;
      const thisPost = event.target.offsetParent.parentElement;
      const currentPostURL = delPostURL + thisPost.dataset.postid;

      if (delBtn.style.display !== "block") {
        delBtn.style.display = "block";
      } else {
        delBtn.style.display = "none";
      }

      delBtn.addEventListener("click", (e) => {
        delBtn.style.display = "none";
        this.deletePost(currentPostURL);
      });
    });
  }

  deletePost(url) {
    const response = confirm("Підтверджуєте видалення поста?"); //запрошую підтвердження видалення
    if (response) {
      try {
        const reply = axios.delete(url); //відправляю запит на видалення (не працює, отримую CORS помилку)

        if (reply.status === 200) {
          this.currentPost.remove(); //якщо запит повернувся з 200, то видаляю пост на сторінці
        }
      } catch (error) {
        console.log("Delete error:", error);
      }
    }
  }
}
//(Функція об'єднання масивів об'єднує так, що втрачаю ID користувача. Як ще можна злити масиви в один не знаю.)
/**
 *
 * @param {array} a1 -the first array of objects
 * @param {string} id1 - the name of the property of each object in a1 that is its id
 * @param {array} a2 - the second array of objects
 * @param {string} id2 - the name of the property of each object in a2 that is its id *
 * @returns {array} combined array of Objects
 * @usage const newArray = combineObjects(array1, "box", array2, "_id");
 */

function combineObjects(a1, id1, a2, id2) {
  // create our new array to return
  var newArray = [];
  // iterate over a1 (the outer loop)
  for (let i = 0, l = a1.length; i < l; i++) {
    let outer = a1[i]; // get the current object for this iteration
    // iterate over a2 (the inner loop)
    for (let j = 0, m = a2.length; j < m; j++) {
      let inner = a2[j]; // get the current object for this iteration of the inner loop
      if (outer[id1] == inner[id2]) {
        // compare the ids for the outer and inner objects
        let o = {}; // create a new blank object to add to newArray
        o[id1] = outer[id1]; // set its id (using the same property name as that of a1)
        for (let prop in outer) {
          // iterate over properties
          if (prop != id1) {
            // ignore the id property, but copy the others
            o[prop] = outer[prop];
          }
        }
        for (let prop in inner) {
          if (prop != id2) {
            o[prop] = inner[prop];
          }
        }
        // add this object to newArray
        newArray.push(o);
      }
    }
  }
  // return newArray
  return newArray;
}

const onClickOutside = (e) => {
  if (!e.target.className.includes("post__more")) {
    const arr = Array.from(document.querySelectorAll(".post__delete"));
    arr.forEach((e) => (e.style.display = "none"));
  }
};
window.addEventListener("click", onClickOutside);

axios.all([axios.get(usersURL), axios.get(postsURL)]).then((responseArr) => {
  const { data: arr1 } = responseArr[0];
  const { data: arr2 } = responseArr[1];
  const combinedArray = combineObjects(arr1, "id", arr2, "userId");

  combinedArray.forEach(({ id, name, email, title, body }) => {
    new Card(id, name, email, title, body).render();
  });
});
