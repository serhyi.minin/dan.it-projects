/*
Дан массив books.

Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
Перед выводом объекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price).
Если какого-то из этих свойств нет, в консоли должна высветиться ошибка с указанием - какого свойства нет в объекте.
Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.*/

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

class ValidationError extends Error {
  /**
   * Creates custom error of ValidationError.
   * @param {string} message
   * @memberof ValidationError
   */
  constructor(message) {
    super(message);
    this.name = "ValidationError";
  }
}

class ShowBook {
  /**
   * Creates an instance of ShowBook.
   * @param {object} books
   * @param {function} areNecessaryFieldsPresent
   * @memberof ShowBook
   */
  constructor(books, areNecessaryFieldsPresent) {
    this.books = books;
    this.container = document.querySelector("#root");
    this.list = document.createElement("ul");
    this.areNecessaryFieldsPresent = areNecessaryFieldsPresent;
  }
  createList(books) {
    for (let key in books) {
      if (books.hasOwnProperty(key)) {
        const listItem = document.createElement("li");
        listItem.innerHTML = `${key}: ${books[key]}`;
        this.list.appendChild(listItem);
      }
    }
  }

  render(selector = ".root") {
    this.createList(this.books);
    document.querySelector(selector).append(this.list);
  }
}
/**
 *
 *
 * @param {string} field1
 * @param {string} field2
 * @param {string} field3
 * @return {function} Closure to make a function with constants for validation keys in the object passed into this function.
 */
function createFilter(field1, field2, field3) {
  return function (object) {
    if (field1 in object && field2 in object && field3 in object) {
      return true;
    } else {
      let fields = [field1, field2, field3];
      for (let i = 0; i <= Object.keys(object).length; i++) {
        console.log(`Missing: ${fields[i]}`);
      }
    }
  };
}

const areNecessaryFieldsPresent = createFilter(`author`, `name`, `price`);

books.forEach((item) => {
  try {
    if (areNecessaryFieldsPresent(item) === true) {
      new ShowBook(item, areNecessaryFieldsPresent(item)).render();
    } else {
      throw new ValidationError("Упс!");
    }
  } catch (error) {
    console.log(`Catch an error`);
  }
});
