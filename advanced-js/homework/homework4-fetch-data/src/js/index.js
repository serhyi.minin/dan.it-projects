const url = "https://ajax.test-danit.com/api/swapi/films";

fetch(url)
  .then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      console.log(`Bad request`);
    }
  })
  .then((result) => {
    const container = document.querySelector(`.posters`);

    result.forEach(({ episodeId, name, characters, openingCrawl }) => {
      if (container) {
        container.insertAdjacentHTML(
          `beforeend`,
          `<div class="movie solid">
          <img
            class="movie__logo"
            src="src/assets/star-wars-logo.png"
            alt="Star Wars Logo"
          />
          <p class="movie__episode">
            Episode &nbsp<span class="movie__episode-id">${episodeId}</span>
          </p>
          <p class="movie__episode-title">${name}</p>
          <ul class="movie__characters${episodeId}">
          Characters:<br>
        </ul>
        <p class="movie__opening-crawl">${openingCrawl}</p>
        </div>`
        );
        const spinner = document.querySelectorAll(".movie");
        spinner.forEach((item) => {
          item.classList.remove(`solid`);
          item.classList.add(`rainbow`);
        });

        const promises = characters.map((url) =>
          fetch(url).then((response) => response.json())
        );

        Promise.allSettled(promises).then((results) =>
          results.forEach(({ status, value: { name } }) => {
            if (status === "fulfilled") {
              document
                .querySelector(`.movie__characters${episodeId}`)
                .insertAdjacentHTML("beforeend", `<li>${name}</li>`);

              spinner.forEach((item) => {
                item.classList.remove(`rainbow`);
                item.classList.add(`solid`);
              });
            }
          })
        );
      }
    });
  });
