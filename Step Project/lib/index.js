/* Tabs in section our-services */
/* variant without data-attributes

const tabContainer = document.querySelector(`.services-tabs`);
const tabList = Array.from(document.querySelectorAll(`.services-tab`));
const contentList = Array.from(document.querySelectorAll(`.card-content`));
let current = contentList[0];
current.style.display = "flex";

tabContainer.addEventListener(`click`, (event) => {
  tabList.forEach((elem) => {
    elem.classList.remove(`active`);
  });
  event.target.classList.add(`active`);
  let index = tabList.indexOf(event.target);
  contentList[index].style.display = "flex";
  current.style.display = `none`;
  current = contentList[index];
});
*/
let currentCategory = "1";

const container = document.querySelector(".services-tabs");

container.addEventListener("click", (event) => {
  currentCategory = event.target.dataset.category;

  const items = document.querySelectorAll(".card-content");
  items.forEach((item) => {
    item.classList.remove("visible");

    if (item.dataset.category === currentCategory) {
      item.classList.add("visible");
    }
  });

  const tabs = document.querySelectorAll("li");
  tabs.forEach((item) => {
    item.classList.remove("active");

    if (item.dataset.category === currentCategory) {
      item.classList.add("active");
    }
  });
});
/* End tabs in section our-services */
/* Our amazing work section */

const images = [
  {
    src: "images/our_amaizing_work/graphic-design1.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design2.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design3.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design4.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design5.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design6.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design7.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design8.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design9.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design10.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design11.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design12.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design13.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design14.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design15.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design16.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design17.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design18.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design19.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design20.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design21.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design22.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design23.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design24.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design25.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design26.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design27.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design28.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design29.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design30.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design31.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design32.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design33.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design34.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design35.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/graphic-design36.jpg",
    category: "Graphic Design",
  },
  {
    src: "images/our_amaizing_work/landing-page1.jpg",
    category: "Landing Pages",
  },
  {
    src: "images/our_amaizing_work/landing-page2.jpg",
    category: "Landing Pages",
  },
  {
    src: "images/our_amaizing_work/landing-page3.jpg",
    category: "Landing Pages",
  },
  {
    src: "images/our_amaizing_work/landing-page4.jpg",
    category: "Landing Pages",
  },
  {
    src: "images/our_amaizing_work/landing-page5.jpg",
    category: "Landing Pages",
  },
  {
    src: "images/our_amaizing_work/landing-page6.jpg",
    category: "Landing Pages",
  },
  {
    src: "images/our_amaizing_work/landing-page7.jpg",
    category: "Landing Pages",
  },
  {
    src: "images/our_amaizing_work/web-design1.jpg",
    category: "Web Design",
  },
  {
    src: "images/our_amaizing_work/web-design2.jpg",
    category: "Web Design",
  },
  {
    src: "images/our_amaizing_work/web-design3.jpg",
    category: "Web Design",
  },
  {
    src: "images/our_amaizing_work/web-design4.jpg",
    category: "Web Design",
  },
  {
    src: "images/our_amaizing_work/web-design5.jpg",
    category: "Web Design",
  },
  {
    src: "images/our_amaizing_work/web-design6.jpg",
    category: "Web Design",
  },
  {
    src: "images/our_amaizing_work/web-design7.jpg",
    category: "Web Design",
  },
  {
    src: "images/our_amaizing_work/wordpress1.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress2.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress3.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress4.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress5.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress6.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress7.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress8.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress9.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress10.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress11.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress12.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress13.jpg",
    category: "Wordpress",
  },
  {
    src: "images/our_amaizing_work/wordpress14.jpg",
    category: "Wordpress",
  },
];

let perPage = 12;
let curCategory = `All`;
let allCards = document.querySelectorAll(`.card`);

const render = (arr) => {
  const mainContainer = document.querySelector(".cards-container");
  const sliceArray = arr.slice(0, perPage);
  const htmlArray = sliceArray.map((element) => {
    return `
        <div class="card">
            <div class="front">
              <img
                src="${element.src}"
                alt="${element.category}"
              />
            </div>
            <div class="back">
              <img src="images/our-amaizing-work-icon.svg" alt="Icon" />
              <h3>creative design</h3>
              <p>${element.category}</p>
            </div>
          </div>`;
  });

  mainContainer.innerHTML = htmlArray.join(" ");
  /* flip card  */
  allCards = document.querySelectorAll(`.card`);
  allCards.forEach((e) => {
    e.addEventListener(`click`, (event) => {
      let element = event.currentTarget;

      if (element.className === "card") {
        if (element.style.transform == "rotateY(180deg)") {
          element.style.transform = "rotateY(0deg)";
        } else {
          element.style.transform = "rotateY(180deg)";
        }
      }
    });
  });

  /* flip end */
  const btn = document.querySelector(".load-more");

  if (perPage >= arr.length) {
    btn.classList.add(`hide`);
  } else {
    btn.classList.remove(`hide`);
  }
};
render(images);
const filteredArr = (arr, category) => {
  return arr.filter((e) => e.category === category);
};

const tabsContainer = document.querySelector(".our-amazing-work-tabs");

tabsContainer.addEventListener("click", (event) => {
  const category = event.target.dataset.category;
  const tabs = document.querySelectorAll(".our-amazing-work-tab");
  curCategory = category;

  perPage = 12;

  if (category === "All" && event.target !== event.currentTarget) {
    render(images);
  } else if (event.target !== event.currentTarget) {
    const newArr = filteredArr(images, category);
    render(newArr);
  }
  tabs.forEach((e) => {
    e.classList.remove("selected");
  });
  if (event.target !== event.currentTarget) {
    event.target.classList.add("selected");
  }
});

const button = document.querySelector(".load-more");

button.addEventListener(`click`, () => {
  perPage = perPage + 12;

  if (curCategory === `All`) {
    render(images);
  } else {
    render(filteredArr(images, curCategory));
  }
});

/* carousel */
const carousel = {
  slideshow: document.querySelector(".slideshow-container"),
  slides: document.querySelectorAll(".mySlides"),
  dotContent: document.querySelector(".dot-content"),
  btnNext: document.querySelector(".next"),
  btnPrev: document.querySelector(".prev"),
  allCustomers: document.querySelectorAll(".slide"),
  imageURL: [
    "images/testimonials/CRISTINA_GOLIS.jpg",
    "images/testimonials/JOAO_PEDRO.jpg",
    "images/testimonials/HASAN_ALI.jpg",
    "images/testimonials/MARIANA_CUNHA.jpg",
    "images/testimonials/NIKOLA_ZYBALA.jpg",
    "images/testimonials/GREG_KHEEL.jpg",
  ],
  quote: [
    `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet
        sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget
        aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem,
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam
        facilisis.`,
    `Two roads diverged in a wood, and I, I took the one less travelled
        by, and that has made all the difference.`,
    `I have a dream that my four little children will one day live in a
        nation where they will not be judged by the color of their skin
        but by the content of their character.`,
    `Ask, and it shall be given you; seek, and you shall find.`,
    `All the world’s a stage, and all the men and women merely players.`,
    `When nothing to say, keep silent.`,
  ],
  author: [
    `CRISTINA GOLIS`,
    `JOAO PEDRO`,
    `HASAN ALI`,
    `MARIANA CUNHA`,
    `NIKOLA ZYBALA`,
    `GREG KHEEL`,
  ],
  position: [
    `Chief Financial Officer`,
    `Company Owner`,
    `UX Designer`,
    `Team Muse`,
    `Creator`,
    `Last in Collection`,
  ],
  slideWidth: 109,
  currentSlide: 0,
  slideIndex: 0,
  dotIndex: 0,
  currentTranslateValue: 0,
  showSlides(slideIndex) {
    const slides = document.querySelectorAll(".mySlides");
    const dots = document.querySelectorAll(".dot");

    if (slideIndex >= slides.length) {
      this.slideIndex = slides.length - 1;
    } else if (slideIndex < 1) {
      this.slideIndex = 0;
    } else {
      this.slideIndex = slideIndex;
    }

    for (let i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }

    for (let i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" top", "");
    }
    slides[this.slideIndex].style.display = "block";
    dots[this.slideIndex].className += " top";
  },
  render(slideIndex) {
    const slides = document.querySelectorAll(".mySlides");

    if (slideIndex >= slides.length) {
      this.slideIndex = slides.length - 1;
    } else if (slideIndex < 1) {
      this.slideIndex = 0;
    } else {
      this.slideIndex = slideIndex;
    }
    const imgArr = this.imageURL.map((e, index) => {
      return `<div class="mySlides fade">
      <h3 class="quote">${this.quote[index]}</h3>
      <p class="author">${this.author[index]}</p>
      <p class="position text">${this.position[index]}</p>
      <img class="imageURL" src="${e}" alt="${this.author[index]}"/>
    </div>
    `;
    });
    this.slideshow.innerHTML = imgArr.join(" ");
    const dotArr = this.imageURL.map((e, index) => {
      return `
      <li class="slide">
      <img
        class="imageURL dot"
        src="${e}"
        alt="${this.author[index]}"
      />
    </li>
    `;
    });
    this.dotContent.innerHTML = dotArr.join(" ");
    this.showSlides(slideIndex);
    this.addEventListeners();

    if (this.slideIndex >= 4) {
      this.translate(-(this.slideWidth * (slideIndex - 4)));
      this.currentSlide = 4;
    } else {
      this.currentSlide = this.slideIndex + 1;
    }
  },
  translate(slideWidth) {
    const slide = document.querySelectorAll(".slide");
    slide.forEach((element, index) => {
      if (index <= slide.length - 1) {
        element.style.transform = `translateX(${slideWidth}px)`;
      }
    });
  },

  nextSlide() {
    this.slides = document.querySelectorAll(".mySlides");

    if (this.slideIndex < this.slides.length - 1) {
      this.slideIndex = ++this.slideIndex;
      this.showSlides(this.slideIndex);
      this.currentSlide = ++this.currentSlide;
    }

    if (this.currentSlide >= 4) {
      this.currentSlide = 4;
      this.currentTranslateValue = -(this.slideIndex - 3) * this.slideWidth;
      this.translate(this.currentTranslateValue);
    }
    this.showSlides(this.slideIndex);
  },
  prevSlide() {
    this.slideIndex = --this.slideIndex;
    this.showSlides(this.slideIndex);

    if (this.currentSlide > 0) {
      this.currentSlide = --this.currentSlide;
    }

    if (
      this.currentSlide < 1 &&
      this.slides.length > 4 &&
      this.currentTranslateValue != 0
    ) {
      this.currentSlide = 1;
      this.currentTranslateValue = this.currentTranslateValue + this.slideWidth;
      this.translate(this.currentTranslateValue);
    }
    this.showSlides(this.slideIndex);

    if (this.slideIndex === 0) {
      this.currentTranslateValue = 0;
    }
  },
  addEventListeners() {
    this.btnPrev.addEventListener("click", this.prevSlide.bind(this));
    this.btnNext.addEventListener("click", this.nextSlide.bind(this));
    this.dots = document.querySelectorAll(".dot");
    this.dots.forEach((element, index) => {
      element.addEventListener("click", () => {
        const dots = document.querySelectorAll(".dot");

        for (let i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" top", "");
        }
        dots[index].className += " top";

        this.currentSlide = index + 1;

        if (index > 4) {
          this.currentSlide = 4;
        }
        this.slideIndex = index;
        this.showSlides(this.slideIndex);

        if (this.currentTranslateValue !== 0) {
          this.currentSlide =
            index + 1 + this.currentTranslateValue / this.slideWidth;
          console.log(this.currentSlide);
        }
      });
    });
  },
};
carousel.render(0);
